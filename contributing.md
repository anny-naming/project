# We are using

* Poetry as dependency management tool.

* Ruff as linter and formater

* Pre-commit for pre commit git hooks

* VS Code as IDE

# Setup

* clone repo
* install [`poetry`](https://python-poetry.org/docs/#installation)
* run `poetry install` for a dependency installation
* run `pre-commit install` to setup hooks

# Setting up VS Code

* Install Ruff [extension](https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff)
